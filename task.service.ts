import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { getStartIndex, PaginationDto } from 'src/dto/pager.dto';
import { RequestMeta } from '../../dto/request-meta.dto';
import { RequestMetaService } from '../../interceptors/request-meta.service';
import { CreateTaskDto, getAllTasksResponseDto } from './task.dto';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';

@Injectable()
export class TaskService {
  constructor(private taskRepository: TaskRepository, private readonly requestMetaService: RequestMetaService) {}

  findOne(id: number) {
    if (!id) {
      return null;
    }
    return this.taskRepository.findOneBy({ id });
  }

  findTasksOfUser(id: string): Promise<Task[]> {
    return this.taskRepository.findTasksOfUser(id);
  }

  async findEmailById(id: number) {
    const task = await this.taskRepository.findOneBy({ id });

    if (!task) {
      throw new NotFoundException(`Task with ID ${id} not found`);
    }

    const user = await task.user;

    if (!user) {
      throw new NotFoundException(`User associated with task with ID ${id} not found`);
    }

    return user.email;
  }

  async createTask(body: CreateTaskDto, request: Request, userId: any) {
    const requestMeta: RequestMeta = await this.requestMetaService.getRequestMeta(request);
    const task = this.taskRepository.create({ ...body, user: userId });

    if (requestMeta.email) {
      return this.taskRepository.save(task);
    }
    throw new UnauthorizedException('User not Authorised');
  }

  async removeTask(taskId: number, request: Request) {
    const requestMeta: RequestMeta = await this.requestMetaService.getRequestMeta(request);
    const userEmail = await this.findEmailById(taskId);
    if (requestMeta.email === userEmail) {
      const task = await this.findOne(taskId);
      if (!task) {
        throw new NotFoundException('Task not found');
      }
      return this.taskRepository.delete(taskId);
    }
    throw new UnauthorizedException('User not Authorised');
  }

  async updateTask(taskId: number, attrs: Partial<Task>, request: Request) {
    const requestMeta: RequestMeta = await this.requestMetaService.getRequestMeta(request);
    const userEmail = await this.findEmailById(taskId);
    if (requestMeta.email === userEmail) {
      const task = await this.findOne(taskId);
      if (!task) {
        throw new NotFoundException('Task not found');
      }
      Object.assign(task, attrs);
      return this.taskRepository.save(task);
    }
    throw new UnauthorizedException('User not Authorised');
  }

  async getAllTasks(
    paginationDto: PaginationDto,
    dueDate: 'ASC' | 'DESC',
    userId: string,
    priority?: string,
    status?: boolean,
  ): Promise<getAllTasksResponseDto> {
    const startIndex = getStartIndex(paginationDto.page, paginationDto.limit);
    return this.taskRepository.getAllTasks(paginationDto, startIndex, dueDate, userId, priority, status);
  }
}
