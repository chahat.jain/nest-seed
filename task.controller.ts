import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Query, Req } from '@nestjs/common';
import { ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { logger } from 'src/config/app-logger.config';
import { PaginationDto } from 'src/dto/pager.dto';
import { RequestMeta } from 'src/dto/request-meta.dto';
import { RequestMetaService } from 'src/interceptors/request-meta.service';
import { CreateTaskDto, getAllTasksResponseDto, TaskDto, UpdateTaskDto } from './task.dto';
import { TaskService } from './task.service';

@Controller('task')
@ApiTags('tasks')
@ApiSecurity('BearerAuthorization')
export class TaskController {
  constructor(private taskService: TaskService, private readonly requestMetaService: RequestMetaService) {}
  @Post('createTask')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    type: TaskDto,
    description: 'Task created successfully',
  })
  async createTask(@Body() body: CreateTaskDto, @Req() request: Request) {
    const requestMeta = await this.requestMetaService.getRequestMeta(request);
    const userId = requestMeta.userId;
    return await this.taskService.createTask(body, request, parseInt(userId));
  }

  @Delete('/:taskId')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Task deleted successfully',
  })
  removeTask(@Param('taskId') taskId: number, @Req() request: Request) {
    return this.taskService.removeTask(taskId, request);
  }

  @Patch('/:taskId')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Task updated successfully',
  })
  updateTask(@Param('taskId') taskId: number, @Body() body: UpdateTaskDto, @Req() request: Request) {
    return this.taskService.updateTask(taskId, body, request);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Showing all tasks of user',
  })
  async findAllTasks(@Req() request: Request) {
    const requestMeta = await this.requestMetaService.getRequestMeta(request);
    const userId: string = requestMeta.userId;
    return this.taskService.findTasksOfUser(userId);
  }

  @Get('/pagination')
  @ApiResponse({
    status: HttpStatus.OK,
    type: getAllTasksResponseDto,
    description: 'UserResponseDTO with user & pagination details',
  })
  async getAllTasks(
    @Query() paginationDto: PaginationDto,
    @Query('priority') priority: string,
    @Query('status') status: boolean,
    @Query('dueDate') dueDate: 'ASC' | 'DESC',
    @Req() request: Request,
  ): Promise<getAllTasksResponseDto> {
    if (!paginationDto.limit) {
      paginationDto.limit = 10;
    }
    if (!paginationDto.page) {
      paginationDto.page = 1;
    }
    const requestMeta: RequestMeta = await this.requestMetaService.getRequestMeta(request);
    logger.debug('requestMeta object ', requestMeta);
    const userId = requestMeta.userId;
    return this.taskService.getAllTasks(paginationDto, dueDate, userId, priority, status);
  }
}
